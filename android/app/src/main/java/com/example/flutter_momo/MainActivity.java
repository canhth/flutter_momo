package com.example.flutter_momo;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import java.util.Base64;

import java.util.HashMap;
import java.util.Map;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodChannel;
import vn.momo.momo_partner.AppMoMoLib;

public class MainActivity extends FlutterActivity {

    MethodChannel.Result pendingResult;

    String orderId;

    // Momo
    private int amount = 1000;
    private String fee = "0";
    int environment = 0; //developer default
    private String merchantName = "ShopApp";
    private String merchantCode = "MOMOSWBC20220817";
    private String merchantNameLabel = "Nhà cung cấp";
    private String description = "Demo Thanh toán Momo";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Momo init
        AppMoMoLib.getInstance().setEnvironment(AppMoMoLib.ENVIRONMENT.DEVELOPMENT);

        // Initialize logger
        Logger.addLogAdapter(new AndroidLogAdapter());
    }

    private String CHANNEL_NAME = "com.example.flutter_momo/payment";

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);

        MethodChannel channel = new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL_NAME);
        channel.setMethodCallHandler((call, result) -> {
            try {
                if (call.method.equals("request_payment")) {
                    requestPayment(result);
                } else if (call.method.equals("encryptRSA")) {
                    encryptDataByRSA(result);
                } else {
                    result.notImplemented();
                }
            } catch (Exception e) {
                result.success(e.getMessage());
            }
        });
    }

    // Get token through MoMo app
    private void requestPayment(MethodChannel.Result result) {
        pendingResult = result;
        orderId = "MM" + System.currentTimeMillis();

        AppMoMoLib.getInstance().setAction(AppMoMoLib.ACTION.PAYMENT);
        AppMoMoLib.getInstance().setActionType(AppMoMoLib.ACTION_TYPE.GET_TOKEN);

        // https://developers.momo.vn/v2/#/docs/app_in_app?id=tham-s%e1%bb%91-t%e1%ba%a1o-deeplink-m%e1%bb%9f-app-momo
        Map<String, Object> eventValue = new HashMap<>();
        // client Required
        eventValue.put("merchantname", merchantName); //Tên đối tác. được đăng ký tại https://business.momo.vn. VD: Google, Apple, Tiki , CGV Cinemas
        eventValue.put("merchantcode", merchantCode); //Mã đối tác, được cung cấp bởi MoMo tại https://business.momo.vn
        eventValue.put("amount", amount); // Kiểu integer
        eventValue.put("orderId", orderId); //uniqueue id cho Bill order, giá trị duy nhất cho mỗi đơn hàng
        eventValue.put("orderLabel", "Mã đơn hàng"); //gán nhãn

        // client Optional - bill info
        eventValue.put("merchantnamelabel", "Dịch vụ"); //gán nhãn
        eventValue.put("fee", fee); //Kiểu integer
        eventValue.put("description", description); //mô tả đơn hàng - short description

        // client extra data
        eventValue.put("requestId", merchantCode + "merchant_billId_" + System.currentTimeMillis());
        eventValue.put("partnerCode", merchantCode);
        eventValue.put("extra", "");

        AppMoMoLib.getInstance().requestMoMoCallBack(this, eventValue);
    }

    // Get token callback from MoMo app an submit to server side
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppMoMoLib.getInstance().REQUEST_CODE_MOMO && resultCode == -1) {
            if (data != null) {
                if (data.getIntExtra("status", -1) == 0) {
                    //TOKEN IS AVAILABLE

                    String token = data.getStringExtra("data"); // Token response
                    String phoneNumber = data.getStringExtra("phonenumber");
                    String env = data.getStringExtra("env");

                    Logger.i("Token: " + token + "\nphoneNumber: " + phoneNumber + "\nenv: " + env);

                    if (env == null) {
                        env = "app";
                    }

                    if (token != null && !token.equals("")) {
                        // TODO: send phoneNumber & token to your server side to process payment with MoMo server
                        // IF Momo topup success, continue to process your order
                        Map<String, Object> returnValue = new HashMap<>();
                        returnValue.put("token", token);
                        returnValue.put("phonenumber", phoneNumber);
                        returnValue.put("orderId", orderId);

                        pendingResult.success(returnValue);

                    } else {
                        onPaymentFailed();
                    }
                } else if (data.getIntExtra("status", -1) == 1) {
                    //TOKEN FAIL
                    onPaymentFailed();
                } else if (data.getIntExtra("status", -1) == 2) {
                    //TOKEN FAIL
                    onPaymentFailed();
                } else {
                    //TOKEN FAIL
                    onPaymentFailed();
                }
            } else {
                onPaymentFailed();
            }
        } else {
            onPaymentFailed();
        }
    }

    private void onPaymentSuccess() {
    }

    private void onPaymentFailed() {
        String notReceiveInfoMessage = "message: Không nhận được thông tin";

        Logger.wtf(notReceiveInfoMessage);

        pendingResult.success(null);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void encryptDataByRSA(MethodChannel.Result result) throws Exception {
        try {
            JSONObject data = new JSONObject();
            data.put("partnerCode","MOMOSWBC20220817");
            data.put("partnerRefId",orderId);
            data.put("amount",amount);

            Logger.i(data.toString());

            final String publicKey = "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAlKWmle6myh1b6BEW0ZtSIZbEyPpp5EVDmLbG6zkbAjQCBZM7tszKr7HNVtuLDa3X16Mawr2QmntVkC/ntIwH+syW78mRUAIxEiBmWwbYf8Ya2+ux+1d4+iGRO9SbD9LuhHoqfABdK1NWCtHrCDJ/IN+YKU2Jb06E0WUvug+WmGJcK507V29tpn5ga09FDfok1FzB55VfTqRwhEdbE191PpfplvtN3ymVwOGmDEBxTTDbpndzu/NcASjpAt5VoZs6eB8VBfANwLeAfx+QG/wjjmQoxNfdWKALFpP0GNHXyIkIniy8x/JdkZINt5wnEtmjD8EhuvJGB3Asj0C7VK2RkuDamlOHoOKJn+wFHYmPjgSJfzN66VotWUNKPUKQVb9RcfR9rmUsaQkfP2tVMXJqxdyj2U03fre6tLUznS7cJaWvk+JTL0DMgdbX+jRE4qQBKF5pat6jTIuT190eYsNRVI8VMjOpkQsNOJcAnK0a3yfWntbakkVojowklFkMkxIjgAl9+a0/xCoLjFg14npsESsus3nikEid2CRp7dGD/JRskVipnCXd3ILi4+h11u2U1OXWBDUDho0B0XNiVmoApdEpP3AIBGc993HOyKZ5qfseEQnbXzwcOOdOd7I1Nw4qVugNXTDOb7eSTkGP1vRj0/pTrfNItjdfIi68TnC1iHcCAwEAAQ==";

            byte[] publicKeyBytes = Base64.getDecoder().decode(publicKey);
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey rsaPublicKey = keyFactory.generatePublic(keySpec);

            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, rsaPublicKey);

            byte[] encryptedData = cipher.doFinal(data.toString().getBytes(StandardCharsets.UTF_8));

            final String dataEncrypted =  Base64.getEncoder().encodeToString(encryptedData);
            result.success(dataEncrypted);
        }
        catch (Exception e) {
            result.success(null);
        }

    }

}
