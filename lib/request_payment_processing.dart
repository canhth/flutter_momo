import 'dart:convert';

import 'package:flutter_momo/endpoint.dart';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';

/// https://developers.momo.vn/v2/#/docs/app_in_app?id=x%e1%bb%ad-l%c3%bd-thanh-to%c3%a1n
class PaymentProcessingRequest {
  final String partnerCode;
  final String partnerRefId;
  final String customerNumber;
  final String appData;
  final String hash;
  final double version;
  final int payType;

  PaymentProcessingRequest({
    required this.partnerCode,
    required this.partnerRefId,
    required this.customerNumber,
    required this.appData,
    required this.hash,
    this.version = 2.0,
    this.payType = 3,
  });

  Map<String, dynamic> toJson() {
    return {
      "partnerCode": partnerCode,
      "partnerRefId": partnerRefId,
      "customerNumber": customerNumber,
      "appData": appData,
      "hash": hash,
      "version": version,
      "payType": payType,
    };
  }
}

class PaymentProcessingResponse {
  final int status;
  final String message;
  final String transid;
  final int amount;
  final String signature;

  PaymentProcessingResponse({
    required this.status,
    required this.message,
    required this.transid,
    required this.amount,
    required this.signature,
  });

  factory PaymentProcessingResponse.fromJson(Map<String, dynamic> json) {
    return PaymentProcessingResponse(
      status: json["status"],
      message: json["message"],
      transid: json["transid"],
      amount: json["amount"],
      signature: json["signature"],
    );
  }
//
}

Future<PaymentProcessingResponse?> requestPaymentProcessing(
    PaymentProcessingRequest request) async {
  final Map<String, String> headers = {
    'Content-Type': 'application/json; charset=UTF-8'
  };

  final body = jsonEncode(request.toJson());

  final logger = Logger();
  logger.v('headers: $headers');
  logger.v('body: $body');

  try {
    final response = await http.Client().post(
      Uri.parse(Endpoint.paymentProcessing),
      headers: headers,
      body: body,
    );

    if (response.statusCode == 200) {
      if (response.body != null) {
        final data = jsonDecode(response.body);

        logger.i('data response: $data');

        return PaymentProcessingResponse.fromJson(data);
      }
    } else {
      return null;
    }
  } catch (e) {
    rethrow;
  }
}
