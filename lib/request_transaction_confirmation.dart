import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';

import 'endpoint.dart';

/// https://developers.momo.vn/v2/#/docs/app_in_app?id=x%c3%a1c-nh%e1%ba%adn-giao-d%e1%bb%8bch
class TransactionConfirmRequest {
  final String partnerCode;
  final String partnerRefId;
  final String requestType;
  final String requestId;
  final String momoTransId;
  final String signature;

  const TransactionConfirmRequest({
    required this.partnerCode,
    required this.partnerRefId,
    required this.requestType,
    required this.requestId,
    required this.momoTransId,
    required this.signature,
  });

  Map<String, dynamic> toJson() {
    return {
      "partnerCode": partnerCode,
      "partnerRefId": partnerRefId,
      "requestType": requestType,
      "requestId": requestId,
      "momoTransId": momoTransId,
      "signature": signature,
    };
  }
}

class TransactionConfirmResponse {
  final int status;
  final String message;
  final DataResponse data;
  final String signature;

  const TransactionConfirmResponse({
    required this.status,
    required this.message,
    required this.data,
    required this.signature,
  });

  factory TransactionConfirmResponse.fromJson(Map<String, dynamic> json) {
    return TransactionConfirmResponse(
      status: json["status"],
      message: json["message"],
      data: DataResponse.fromJson(json["data"]),
      signature: json["signature"],
    );
  }
//
}

class DataResponse {
  final String partnerCode;
  final String partnerRefId;
  final String momoTransId;
  final int amount;

  const DataResponse({
    required this.partnerCode,
    required this.partnerRefId,
    required this.momoTransId,
    required this.amount,
  });

  factory DataResponse.fromJson(Map<String, dynamic> json) {
    return DataResponse(
      partnerCode: json["partnerCode"],
      partnerRefId: json["partnerRefId"],
      momoTransId: json["momoTransId"],
      amount: json["amount"],
    );
  }
//
}

Future<TransactionConfirmResponse?> requestTransactionConfirm(
  TransactionConfirmRequest request,
) async {
  final Map<String, String> headers = {
    'Content-Type': 'application/json; charset=UTF-8'
  };

  final body = jsonEncode(request.toJson());

  final logger = Logger();
  logger.v('headers: $headers');
  logger.v('body: $body');

  try {
    final response = await http.Client().post(
      Uri.parse(Endpoint.transactionConfirm),
      headers: headers,
      body: body,
    );

    if (response.statusCode == 200) {
      if (response.body != null) {
        final data = jsonDecode(response.body);

        logger.i('data response: $data');

        return TransactionConfirmResponse.fromJson(data);
      }
    } else {
      return null;
    }
  } catch (e) {
    rethrow;
  }
}
