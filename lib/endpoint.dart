class Endpoint {
  static const domain = 'https://test-payment.momo.vn';
  static const paymentProcessing = '$domain/pay/app';
  static const transactionConfirm = '$domain/pay/confirm';
}
