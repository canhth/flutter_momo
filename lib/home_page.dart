import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_momo/request_payment_processing.dart';
import 'package:flutter_momo/request_transaction_confirmation.dart';
import 'package:flutter_momo/utils.dart';
import 'package:logger/logger.dart';

import 'momo_config.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isActiveBtn = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Thanh toán Momo'),
      ),
      body: Center(
        child: FilledButton(
          onPressed: isActiveBtn ? _requestPayment : null,
          child: const Text('Request payment'),
        ),
      ),
    );
  }

  Future<void> _requestPayment() async {
    setState(() {
      isActiveBtn = false;
    });

    const channel = MethodChannel('com.example.flutter_momo/payment');

    try {
      final result = await channel.invokeMethod('request_payment');

      Logger().i(result);

      if (result != null) {
        final token = result['token'];
        final phoneNumber = result['phonenumber'];
        final partnerRefId = result['orderId'];
        const partnerCode = MomoConfig.partnerCode;

        final hash = await channel.invokeMethod('encryptRSA');

        Logger().v("hash: $hash");

        final paymentProcessingRequest = PaymentProcessingRequest(
          partnerCode: partnerCode,
          partnerRefId: partnerRefId,
          customerNumber: phoneNumber,
          appData: token,
          hash: hash,
        );

        final paymentProcessingResponse =
            await requestPaymentProcessing(paymentProcessingRequest);
        if (paymentProcessingResponse != null) {
          final requestId = DateTime.now().millisecondsSinceEpoch.toString();
          const requestType = 'capture';
          final momoTransId = paymentProcessingResponse.transid;

          final inputData =
              'partnerCode=$partnerCode&partnerRefId=$partnerRefId&requestType=$requestType&requestId=$requestId&momoTransId=$momoTransId';
          final signature = getSignatureByHmacSHA256(inputData);

          final transactionConfirmRequest = TransactionConfirmRequest(
            partnerCode: partnerCode,
            partnerRefId: partnerRefId,
            requestType: requestType,
            requestId: requestId,
            momoTransId: momoTransId,
            signature: signature,
          );
          final transactionConfirmResponse =
              await requestTransactionConfirm(transactionConfirmRequest);

          if (transactionConfirmResponse != null && mounted) {
            ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(content: Text('Thanh toán thành công')));
          }
        }
      }
    } catch (e) {
      Logger().e(e);
    } finally {
      setState(() {
        isActiveBtn = true;
      });
    }
  }
}
