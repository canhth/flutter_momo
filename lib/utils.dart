import 'dart:convert';

import 'package:crypto/crypto.dart';

import 'momo_config.dart';

String getSignatureByHmacSHA256(String data) {
  try {
    final key = utf8.encode(MomoConfig.secretKey);
    final bytes = utf8.encode(data);

    final hmacSha256 = Hmac(sha256, key);
    return hmacSha256.convert(bytes).toString();
  } catch (e) {
    rethrow;
  }
}

// Future<String> getHashByRSA(String data) async {
//   try {
//     final result = await RSA.encryptPKCS1v15(
//       jsonEncode(data),
//       MomoConfig.publicKey,
//     );
//
//     return result;
//   } catch (e) {
//     rethrow;
//   }
// }
